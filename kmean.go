package main

import (
	"math"
)

var clusters []*Cluster

func MakeClusters(clusterCount int, dataPoints []*Point) []Cluster {
	clusters = make([]*Cluster, clusterCount)

	for i := range clusters {
		cluster := NewCluster(dataPoints[0].Dimensions())
		center := dataPoints[(len(dataPoints)*i)/clusterCount]
		copy(cluster.Centroid.Coordinates, center.Coordinates)
		clusters[i] = cluster
	}

	for i := 0; ; i++ {
		for _, cluster := range clusters {
			cluster.ClearPoints()
		}

		for i := 0; i < len(dataPoints); i++ {
			dataPoint := dataPoints[i]
			var nearestCluster *Cluster
			minDistance := math.MaxFloat64
			for _, cluster := range clusters {
				distance := CalcDistance(cluster.Centroid, dataPoint)
				if distance < minDistance {
					minDistance = distance
					nearestCluster = cluster
				}
			}
			if nearestCluster != nil {
				nearestCluster.AddPoint(dataPoint)
			}
		}

		hasChanged := false
		for i = 0; i < len(clusters); i++ {
			cluster := clusters[i]
			// pt is a copy of the centroid before updating
			pt := *cluster.Centroid
			cluster.UpdateCentroid()
			hasChanged = hasChanged || !pt.Equal(cluster.Centroid)
		}
		if !hasChanged {
			//fmt.Fprintf(os.Stderr, "Iterations=%d\n\n", i)
			break
		}
	}

	ret := make([]Cluster, len(clusters))
	for i, clusterPtr := range clusters {
		ret[i] = *clusterPtr
	}
	return ret
}
