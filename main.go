package main

import (
	"os"
	"fmt"
	"flag"
	"io"
	"encoding/json"
	"strings"
)

var (
	ptParser        = PointParser{}
	opts            = programOpts{}
	outputProcessor func([]Cluster) io.Reader
)

type programOpts struct {
	isStdIn      bool
	inputFile    string
	separator    string
	outputMode   string
	clusterCount uint
}

func main() {
	flag.StringVar(&opts.inputFile, "f", "", "Defines points input")
	flag.StringVar(&opts.separator, "s", ",",
		"Defines the delimiter/separator of coordinates",
	)
	flag.UintVar(&opts.clusterCount, "clusters", 2, "Defines how many clusters to make")
	flag.StringVar(&opts.outputMode, "out", "json", "Defines format of output")
	flag.Parse()

	switch opts.outputMode {
	case "json":
		outputProcessor = getJSONOutputProcessor(false)
	case "jsonPretty":
		outputProcessor = getJSONOutputProcessor(true)
	case "csv":
		outputProcessor = getCSVOutputProcessor()
	}

	switch {
	case len(opts.inputFile) != 0:
		fromFile(opts.inputFile)
	default:
		opts.isStdIn = true
		fromStdIn()
	}
}

func fromStdIn() {
	input := os.Stdin
	processInput(input)
}

func fromFile(file string) {
	f, err := os.Open(file)
	if err != nil {
		fmt.Printf("Error in finding file: %v", err)
	}

	processInput(f)
}

func processInput(input io.Reader) {
	ptsReader := ptParser.ParseReader(input)
	pts := make([]*Point, 0, 0)
	for pt := range ptsReader {
		pts = append(pts, pt)
	}

	clusters := MakeClusters(int(opts.clusterCount), pts)
	if outputProcessor != nil {
		processor := outputProcessor(clusters)
		if _, err := io.Copy(os.Stdout, processor); err != nil {
			panic(err)
		}
	}
}

func getJSONOutputProcessor(isPretty bool) func([]Cluster) io.Reader {
	marshaler := json.Marshal
	if isPretty {
		marshaler = func(v interface{}) ([]byte, error) {
			return json.MarshalIndent(v, "", " ")
		}
	}

	jsonOutputProcessor := func(clusters []Cluster) io.Reader {
		newClusters := make([]Cluster, 0, len(clusters))
		for _, cluster := range clusters {
			if len(cluster.DataPoints) > 0 {
				newClusters = append(newClusters, cluster)
			}
		}
		clusters = newClusters

		bytes, err := marshaler(clusters)
		if err != nil {
			panic(err)
		}
		return strings.NewReader(string(bytes))
	}

	return jsonOutputProcessor
}
