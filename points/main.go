package main

import (
	"flag"
	"math/rand"
	"time"
	"fmt"
	"strings"
)

func main() {
	var count, dimensions uint
	flag.UintVar(&count, "c", 10, "Count of points to generate")
	flag.UintVar(&dimensions, "d", 2, "Number of dimensions (point dimensions)")
	flag.Parse()

	rand.Seed(time.Now().UnixNano())

	template := strings.Repeat("%.2f,", int(dimensions))
	template = template[:len(template)-1] + "\n"

	rands := make([]interface{}, dimensions)
	for i := uint(0); i < count; i++ {
		for i := range rands {
			rands[i] = rand.Float64() * 1000
		}
		fmt.Printf(template, rands...)
	}
}
