package main

import (
	"fmt"
	"io"
	"strings"
	"bufio"
	"strconv"
)

func getCSVOutputProcessor() func([]Cluster) io.Reader {
	separator := "\t"

	return func(clusters []Cluster) (reader io.Reader) {
		pipeReader, pipeWriter := io.Pipe()
		reader = pipeReader

		output := bufio.NewWriterSize(pipeWriter, 1024)

		go func() {
			defer pipeWriter.Close()

			dimensions := clusters[0].Centroid.Dimensions()
			csvTemplate := "%v" + separator
			csvTemplate += strings.Repeat("%.3f"+separator, dimensions)
			csvTemplate += "%v" + separator + "%v\n"

			output.Write([]byte("ID" + separator))
			for i := 0; i < dimensions; i++ {
				output.WriteString("Dim")
				output.WriteString(strconv.Itoa(i))
				output.WriteString(separator)
			}
			output.WriteString("Is Centroid ?" + separator + "ClusterID")
			output.WriteRune('\n')

			printPoint := func(p *Point, clusterPt *Point) {
				args := make([]interface{}, 0, len(p.Coordinates)+2)

				args = append(args, p.ID)
				for _, coordinate := range p.Coordinates {
					args = append(args, coordinate)
				}
				args = append(args, p.Tag == PointTagCentroid, clusterPt.ID)

				output.WriteString(fmt.Sprintf(csvTemplate, args...))
				if err := output.Flush(); err != nil {
					panic(err)
				}
			}

			for _, cl := range clusters {
				printPoint(cl.Centroid, cl.Centroid)
				for _, pt := range cl.DataPoints {
					if pt.ID != cl.Centroid.ID {
						printPoint(pt, cl.Centroid)
					}
				}
			}
		}()

		return
	}
}
