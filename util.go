package main

import "sync"

func ParallelFor(from int, to int, f func(int)) {
	wg := new(sync.WaitGroup)
	for i := from; i <= to; i++ {
		wg.Add(1)
		go func(i int) {
			f(i)
			wg.Done()
		}(i)
	}
	wg.Wait()
}

func ParallelWorkerFor(workerCount, from, to int, f func(int)) {
	if from > to {
		from, to = to, from
	}
	jobCount := to - from
	jobQueue := make(chan int, workerCount)
	wg := new(sync.WaitGroup)
	wg.Add(jobCount)

	for i := 0; i < workerCount; i++ {
		go func(i int) {
			for jobIndex := range jobQueue {
				f(jobIndex)
				wg.Done()
			}
		}(i)
	}

	for i := 0; i < jobCount; i++ {
		jobQueue <- i
	}
	close(jobQueue)

	wg.Wait()
}
