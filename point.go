package main

import (
	"sync/atomic"
	"math"
	"fmt"
)

const (
	PointTagData     = iota + 1
	PointTagCentroid
)

var (
	clusterID uint64 = 0
)

type Point struct {
	ID          uint
	Coordinates []float64
	Tag         int
}

func (p *Point) Equal(p2 *Point) bool {
	if len(p.Coordinates) != len(p2.Coordinates) {
		return false
	}

	for i := range p.Coordinates {
		if p.Coordinates[i] != p2.Coordinates[i] {
			return false
		}
	}

	return true
}

func (p *Point) Dimensions() int {
	return len(p.Coordinates)
}

type Cluster struct {
	ID         uint
	Centroid   *Point
	DataPoints []*Point
}

func NewCluster(dimensions int) *Cluster {
	newID := uint(atomic.AddUint64(&clusterID, 1))
	return &Cluster{
		ID:         newID,
		DataPoints: make([]*Point, 0, 0),
		Centroid: &Point{
			ID:  newID,
			Tag: PointTagCentroid,
			Coordinates: make([]float64, dimensions),
		},
	}
}

func (c *Cluster) UpdateCentroid() {
	numOfPoints := float64(len(c.DataPoints))
	if numOfPoints == 0 {
		return
	}

	centroidCoordinates := c.Centroid.Coordinates
	for i := range centroidCoordinates {
		centroidCoordinates[i] = 0
	}

	for i := range c.DataPoints {
		p := c.DataPoints[i]
		if p == nil {
			fmt.Printf("Bad datapoint found on %d", i)
		}

		for j, value := range p.Coordinates {
			centroidCoordinates[j] += value
		}
	}

	for i, value := range centroidCoordinates {
		centroidCoordinates[i] = value / numOfPoints
	}
}

func (c *Cluster) AddPoint(p *Point) {
	c.DataPoints = append(c.DataPoints, p)
}
func (c *Cluster) ClearPoints() {
	c.DataPoints = make([]*Point, 0)
}

func CalcDistance(p1 *Point, p2 *Point) float64 {
	if len(p1.Coordinates) != len(p2.Coordinates) {
		panic("Different dimension count")
	}

	var sumOfPowers float64
	for i := range p1.Coordinates {
		sumOfPowers += math.Pow(p1.Coordinates[i] - p2.Coordinates[i], 2)
	}

	return math.Sqrt(sumOfPowers)
}
