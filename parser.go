package main

import (
	"strings"
	"strconv"
	"io"
	"bufio"
	"sync/atomic"
)

type PointParser struct {
	lastID uint64
}

func (parser *PointParser) getID() uint {
	return uint(atomic.AddUint64(&parser.lastID, 1))
}

func (parser *PointParser) ParseString(lines string) []*Point {
	reader := strings.NewReader(lines)
	expectedSize := len(lines)

	pointChan := parser.ParseReader(reader)

	points := parser.PointChanToSlice(expectedSize, pointChan)

	return points
}

func (parser *PointParser) PointChanToSlice(expectedSize int, pointChan <-chan *Point) []*Point {
	points := make([]*Point, 0, expectedSize)
	for point := range pointChan {
		points = append(points, point)
	}
	return points
}

func (parser *PointParser) ParseReader(reader io.Reader) <-chan *Point {
	pts := make(chan *Point)

	go func() {
		sc := bufio.NewScanner(reader)
		for sc.Scan() {
			line := sc.Text()
			if pt := parser.ParseLine(line); pt != nil {
				pts <- pt
			}
		}
		close(pts)
	}()

	return pts
}

func (parser *PointParser) ParseLine(line string) *Point {
	parts := strings.SplitN(line, ",", -1)
	coordinates := make([]float64, len(parts))

	for i, part := range parts {
		coordinate, err := strconv.ParseFloat(part, 64)
		if err != nil {
			return nil
		}
		coordinates[i] = coordinate
	}

	return &Point{
		ID:          parser.getID(),
		Coordinates: coordinates,
		Tag:         PointTagData,
	}
}
