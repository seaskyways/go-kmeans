package main

import (
	"github.com/sbinet/go-gnuplot"
	"encoding/csv"
	"os"
	"strconv"
	"fmt"
)

func main() {
	plotter, _ := gnuplot.NewPlotter("", true, false)

	file, err := os.Open("/home/ssw/Documents/University/OSS/Exam/clustered.tsv")
	if err != nil {
		panic(err)
	}
	reader := csv.NewReader(file)
	reader.Comma = '\t'

	records, err := reader.ReadAll()
	if err != nil {
		panic(err)
	}

	type cluster struct {
		points [][]float64
	}

	clusters := map[string]cluster{}

	for _, values := range records[1:] {
		clusterID := values[4]
		if _, ok := clusters[clusterID]; !ok {
			clusters[clusterID] = cluster{
				points: [][]float64{{},{}},
			}
		}

		v1, _ := strconv.ParseFloat(values[1], 64)
		v2, _ := strconv.ParseFloat(values[2],64)

		clusters[clusterID].points[0] = append(clusters[clusterID].points[0], v1)
		clusters[clusterID].points[1] = append(clusters[clusterID].points[1], v2)
	}

	for clID , cl := range clusters {
		plotter.PlotNd(
			fmt.Sprintf("Cluster%s", clID),
			cl.points...,
		)
	}


}
